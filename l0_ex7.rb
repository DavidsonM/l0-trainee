n = 0
while n < 1 || n > 10
  print "Calcular a tabuada de:  "
  n = gets.to_i
  if n < 1 || n > 10
    puts "Valor inválido! Digite um número entre 1 e 10"
  end
end
for i in 1..9
  print n*i
  print ", "
end
puts 10*n